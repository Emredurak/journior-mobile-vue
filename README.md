# Journior

> Journior mobile application.

## Usage

``` bash
# Install dependencies
npm install

# Preview on device
ns preview

# Build, watch for changes and run the application
ns run <platform>

# Build, watch for changes and debug the application
ns debug <platform>

# Build for production
ns build <platform> --env.production

```

## Notes

>IOS version still has build problems. Project is working on android properly.
