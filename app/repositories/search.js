import Vue from "nativescript-vue";

export default {
    async searchNews(term){
        return await Vue.prototype.$http.get("/api/search/agencynews/",{search:term});
    },
    async searchUsers(term){
        return await Vue.prototype.$http.get("/api/search/user/",{search:term});
    },
}