import Vue from "nativescript-vue";
import {UploadCallbackHandler} from "../webServices/webHandler";

export default {
    async login(username, password){
        return await Vue.prototype.$http.post("/accounts/user/login/", {username:username, password:password});
    },
    async signup(email, password){
        return await Vue.prototype.$http.post("/accounts/user/", {email:email, password:password});
    },
    async passwordReset(email){
        return await Vue.prototype.$http.post("/accounts/password_reset/password_reset_request/", {email:email});
    },
    async refreshToken(refresh_token){
        return await Vue.prototype.$http.post("/accounts/user/refresh/", {refresh_token:refresh_token});
    },
    async updateProfileData(bio=null, first_name=null, last_name=null, notification_comments=null,
                            notification_news=null, notification_email=null){

        let payload = {};
        if(bio){
            payload.bio = bio;
        }
        if(first_name){
            payload.first_name = first_name;
        }
        if(last_name){
            payload.last_name = last_name;
        }
        if(notification_comments){
            payload.notification_comments = notification_comments;
        }
        if(notification_news){
            payload.notification_news = notification_news;
        }
        if(notification_email){
            payload.notification_email = notification_email;
        }

        return await Vue.prototype.$http.post("/accounts/manage/update_profile/", payload);

    },
    async getAccountDetails(){
        return await Vue.prototype.$http.get("/accounts/manage/", );
    },
    uploadProfilePicture(profilePicturePath, completeHandler){
        const handler = new UploadCallbackHandler();
        handler.errorHandler = (e)=>{
            Vue.prototype.$showError("There was an error while uploading profile picture.")
        }
        handler.completeHandler = (e)=>{
            if(completeHandler){
                try{
                    completeHandler(e);
                }
                catch(exc){
                    console.error(exc);
                }
            }
        }

        Vue.prototype.$http.uploadFile("/accounts/manage/update_picture/", profilePicturePath,
            handler, "image/jpeg", "picture_raw");
    }
}