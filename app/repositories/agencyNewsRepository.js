import Vue from "nativescript-vue";

export default {
    async getAgencyNews(minScore=null, maxScore=null){
        let payload = {};
        if(minScore){
            payload = Object.assign(payload, {min_score:minScore});
        }
        if(maxScore) {
            payload = Object.assign(payload, {max_score: maxScore});
        }
        return await Vue.prototype.$http.get("/api/agency_news/", payload);
    },
    async likeAgencyNews(newsID){
        return await Vue.prototype.$http.post("/api/fav/agencynews/favorite/",{favs:newsID});
    },
    async dislikeAgencyNews(newsID){
        return await Vue.prototype.$http.post("/api/fav/agencynews/unfavorite/",{favs:newsID});
    }
}