import Vue from "nativescript-vue";

export default {
    async getCategories(){
        return await Vue.prototype.$http.get("/api/category/get_all_categories/");
    },
}