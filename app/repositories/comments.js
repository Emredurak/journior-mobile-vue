import Vue from "nativescript-vue";

export default {
    async getCommentsofNews(newsId){
        return await Vue.prototype.$http.get("/api/comments/of_news/",{news_id:newsId});
    },
    async getParentCommentsofNews(newsId){
        return await Vue.prototype.$http.get("/api/comments/parent_comments/",{news_id:newsId});
    },
    async getChildCommentsOf(commentId){
        return await Vue.prototype.$http.get("/api/comments/of_comment/",{comment_id:commentId});
    },
    async getAllRepliesOf(commentId){
        return await Vue.prototype.$http.get("/api/comments/all_children/",{comment_id:commentId});
    },
    async createSingleComment(content, newsId){
        return await Vue.prototype.$http.post("/api/comments/",
            {
                text:content,
                news:newsId,
            });
    },
    async createSingleReply(content, newsId, parent){
        return await Vue.prototype.$http.post("/api/comments/reply/",
            {
                text:content,
                news:newsId,
                parent:parent,
            });
    },
    async deleteComment(commentId){
        return await Vue.prototype.$http.delete("/api/comments/"+commentId+"/");
    },
}