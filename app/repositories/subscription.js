import Vue from "nativescript-vue";

export default {
    async getMySubscriberList(){
        return await Vue.prototype.$http.get("/accounts/subscription/my_subscribers/",);
    },
    async getMySubscribedList(){
        return await Vue.prototype.$http.get("/accounts/subscription/my_subscriptions/",);
    },
    async getSubscribedListOfAnyone(username){
        return await Vue.prototype.$http.get("/accounts/subscription/subscription_of_others/",{username:username});
    },
    async getSubscriberListOfAnyone(username){
        return await Vue.prototype.$http.get("/accounts/subscription/subscribers_of_others/",{username:username});
    },
    async getAnyUserById(userId){
        return await Vue.prototype.$http.get("/accounts/manage/getAnyUsers",{userId:userId});
    },
    async getAnyUserByUsername(username){
        return await Vue.prototype.$http.get("/accounts/profile/get_profile_of/",{username:username});
    },
    async follow(followingId){
        return await Vue.prototype.$http.post("/accounts/subscription/follow/", {following:followingId});
    },
    async unfollow(followingId){
        return await Vue.prototype.$http.post("/accounts/subscription/unfollow/", {following:followingId});
    },
    async blockUser(followerId){
        return await Vue.prototype.$http.post("/api/block/block/", {blocks:followerId});
    },
    async unblockUser(followerId){
        return await Vue.prototype.$http.post("/api/block/unblock/", {blocks:followerId});
    },

}