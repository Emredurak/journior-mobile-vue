import Vue from "nativescript-vue";

export default {
    async getAnyUserById(userId){
        return await Vue.prototype.$http.get("/accounts/manage/getAnyUsers",{userId:userId});
    },
    async getAnyUserByUsername(username){
        return await Vue.prototype.$http.get("/accounts/profile/get_profile_of/",{username:username});
    }

}