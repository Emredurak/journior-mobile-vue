import Vue from "nativescript-vue";
import { localize } from "nativescript-localize";

export default {
    methods: {
        L: (str)=>{
            return localize(str);
        }
    }
};