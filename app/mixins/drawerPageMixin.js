import {Page} from "@nativescript/core/ui/page";
import * as utils from "../shared/utils";


export const DrawerSetup = {
    mounted(){
        // This mixin can only work default.
        if(this.$el.nativeView.page) {
            if(!this.drawer){
                this.$el.nativeView.page.on(Page.loadedEvent, () => {
                    this.$store.commit("disableNavigationGestures");
                    utils.closeDrawer();
                });
            }
            else{
                this.$el.nativeView.page.on(Page.loadedEvent, () => {
                    this.$store.commit("enableNavigationGestures");
                    utils.closeDrawer();
                });
            }
        }
    }
};
