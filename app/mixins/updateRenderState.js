import Vue from "nativescript-vue";

export default {
        methods: {
            updateParentRender(){
                setTimeout(()=>{
                    this.$store.commit("disableRenderBackStackPage");
                    this.$nextTick(()=>{
                        this.$store.commit("enableRenderBackStackPage");
                    });
                }, 1);

            }
        }
    };
