import Vue from "nativescript-vue";
import App from "./components/App";
import Home from "./components/Home";
import { SnackBar, } from "@nstudio/nativescript-snackbar";
import Login from "./components/Login";
import DrawerContent from "./components/DrawerContent";
import RadSideDrawer from "nativescript-ui-sidedrawer/vue";
import RadListView from 'nativescript-ui-listview/vue';
import TextFieldPlugin from '@nativescript-community/ui-material-textfield/vue';
import TextViewPlugin from '@nativescript-community/ui-material-textview/vue';
import Vuex from "vuex";
import {HttpHandler} from "./webServices/webHandler";
import {SecureStorage} from 'nativescript-secure-storage';
import AuthorizationCheckHook from './app_hooks/authorizationCheckResponseHook';
import {tokenSecureKeyName, refreshTokenSecureKeyName} from "./persistence/config";
import StandardPost from "./components/postTemplates/StandardPost";
import AgencyPost from "./components/postTemplates/AgencyPost";
import CouchBaseCache from "./persistence/couchBaseCache";
import {ImageCacheIt} from "@triniwiz/nativescript-image-cache-it";
import { init } from 'nativescript-advanced-webview';
init();
import {setHttpBearerToken, removeHttpBearerToken} from "./webServices/helpers";
import * as imagepicker from "@nativescript/imagepicker";
import * as bottomSheet from "@nativescript-community/ui-material-bottomsheet";
import BottomSheetPlugin from '@nativescript-community/ui-material-bottomsheet/vue';
import * as camera from "@nativescript/camera";
import * as imageCropper from "nativescript-imagecropper";
import {ApplicationBackButtonHook} from "./app_hooks/applicationBackButtonHook";
import RipplePlugin from '@nativescript-community/ui-material-ripple/vue';
import helpers from './helpers'

bottomSheet.install();
Vue.registerElement("ImageCacheIt", ()=>ImageCacheIt);
Vue.registerElement('Carousel', () => require('nativescript-carousel').Carousel);
Vue.registerElement('CarouselItem', () => require('nativescript-carousel').CarouselItem);
Vue.registerElement('VideoPlayer',()=> require('nativescript-videoplayer').Video);
Vue.registerElement('CardView', () => require('@nstudio/nativescript-cardview').CardView
);
Vue.use(BottomSheetPlugin);
Vue.use(RipplePlugin);


CouchBaseCache.setUpCache();
ApplicationBackButtonHook.initiate();
//Setup http handler
const httpModule = new HttpHandler();
httpModule.addResponseMiddleware(AuthorizationCheckHook);
Vue.prototype.$http=httpModule;
Vue.prototype.$camera = camera;
Vue.prototype.$imageCropper = imageCropper;
// Add secure storage
Vue.prototype.$secureStorage = new SecureStorage();
Vue.prototype.$imagePicker = new imagepicker.ImagePicker({
    mode: "single",
    mediaType: imagepicker.ImagePickerMediaType.Image
});

// for store global functions use helpers.js as plugin
// define global helper functions in helpers.js file
// use it in components as 'this.$helpers.firstLetter()' for example
const plugin = {
    install () {
        Vue.helpers = helpers
        Vue.prototype.$helpers = helpers
    }
}
Vue.use(plugin)


// Remove before flight
// Vue.prototype.$secureStorage.removeAllSync();

Vue.use(Vuex);
httpModule.setHeader("Accept", "application/json");
//Setup error and message
const snackbar = new SnackBar();
Vue.prototype.$snackbar = snackbar;
Vue.prototype.$showInfo = (message, button)=>{
    return new Promise((resolve, reject)=>{
        snackbar.action({
            snackText: message,
            textColor: "black",
            actionText:button?button:"Tamam",
            actionTextColor: "#9b002b",
            backgroundColor: "white",
            hideDelay: 5001
        }).then((m)=>resolve(m))
            .catch((e)=>{
                console.error(e);
                resolve(null);
            });
    });
};

Vue.prototype.$showError = (errorText, button)=>{
    return new Promise((resolve, reject)=>{
        snackbar.action({
            snackText: errorText,
            textColor: "white",
            actionText:button?button:"Tamam",
            actionTextColor: "#e6537f",
            backgroundColor: "#3f3f3f",
            hideDelay: 5000
        }).then((m)=>resolve(m))
            .catch((e)=>{
                console.error(e);
                resolve(null);
            });
    });
};

import {getAppState} from "./store/generalStore";
import {registerElement} from "nativescript-vue-template-compiler";

const platform = require("@nativescript/core/platform");


if(platform.isAndroid){

}

//Check login
//
const token = Vue.prototype.$secureStorage.getSync({key: tokenSecureKeyName});
const refreshToken = Vue.prototype.$secureStorage.getSync({key: refreshTokenSecureKeyName});
const shouldLogin = !Boolean(token&&refreshToken);
if(!shouldLogin){
    setHttpBearerToken(token);
}

if(TNS_ENV !== 'production') {

}
Vue.use(RadSideDrawer);
Vue.use(RadListView);
Vue.use(TextFieldPlugin);
Vue.use(TextViewPlugin);
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production');

Vue.component("StandardPost", StandardPost);
Vue.component("AgencyPost", AgencyPost);

new Vue({
    store: getAppState(!shouldLogin, token, refreshToken),
  render (h) {
    return h(
        App,
        [
          h(DrawerContent, { slot: 'drawerContent' }),
          h((shouldLogin)?Login:Home, { slot: 'mainContent' })
        ]
    )
  }
}).$start();
Vue.prototype.$store = getAppState(!shouldLogin, token, refreshToken);


