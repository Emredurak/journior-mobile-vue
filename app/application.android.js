const superProto = android.app.Application.prototype;

// the first parameter of the `extend` call defines the package and the name for the native *.JAVA file generated.
android.app.Application.extend("com.journior.journiormobile.MainApplication", {
    //interfaces: [co.fitcom.fancycamera.CameraProvider],
    onCreate: function() {
        superProto.onCreate.call(this);

        // At this point modules have already been initialized

        // Enter custom initialization code here
    },
    attachBaseContext: function(base) {
        superProto.attachBaseContext.call(this, base);
        // This code enables MultiDex support for the application (if needed compile androidx.multidex:multidex)
        // androidx.multidex.MultiDex.install(this);
    }/*,
    getCameraXConfig: function(){
        return co.fitcom.fancycamera.FancyCamera.defaultConfig(this)
    }*/
});