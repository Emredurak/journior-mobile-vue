import {Frame} from "@nativescript/core/ui/frame/";
import {isAndroid} from "@nativescript/core/platform/";
import {AndroidApplication, AndroidActivityBackPressedEventData} from "@nativescript/core/application";

export class ApplicationBackButtonHook{
    public static initiate(){
        if(isAndroid) {
            this._backButton();
        }
    }
    private static _backButton():void{
        // @ts-ignore
        AndroidApplication.on("activityBackPressed", (args:AndroidActivityBackPressedEventData) => {
            const currentView = Frame.topmost().currentPage;
            if (currentView.hasListeners('backbuttonpressed')) {
                args.cancel = true;
                currentView.notify({
                    eventName: 'backbuttonpressed',
                    object: currentView
                });
            } else
                args.cancel = false;

            });
    }
}