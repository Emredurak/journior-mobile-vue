import {getAppState} from "../store/generalStore";

const AuthorizationCheckHook = (response)=>{
    if(response.statusCode===401){
        const store = getAppState();
        store.dispatch("logout");
    }
}

export default AuthorizationCheckHook;