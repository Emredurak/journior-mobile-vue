import Vue from "nativescript-vue";
import {ServerError, ClientError} from './exceptions';
import WebServiceConfig from "./webServiceConfig";
import {getAppState} from "../store/generalStore";


export class UploadCallbackHandler{
    private _progressHandler:(e)=>void = ()=>{}
    private _errorHandler:(e)=>void = ()=>{}
    private _respondedHandler:(e)=>void = ()=>{}
    private _completeHandler:(e)=>void = ()=>{}
    private _cancelledHandler:(e)=>void = ()=>{}

    constructor(progressHandler?:(e)=>void,
                errorHandler?:(e)=>void,
                respondedHandler?:(e)=>void,
                completeHandler?:(e)=>void,
                cancelledHandler?:(e)=>void) {
        this._progressHandler = progressHandler?progressHandler:this._progressHandler;
        this._errorHandler = errorHandler?errorHandler:this._errorHandler;
        this._respondedHandler = respondedHandler?respondedHandler:this._respondedHandler;
        this._completeHandler = completeHandler?completeHandler:this._completeHandler;
        this._cancelledHandler = cancelledHandler?cancelledHandler:this._cancelledHandler;
    }

    set progressHandler(value: (e) => void) {
        this._progressHandler = value;
    }

    set errorHandler(value: (e) => void) {
        this._errorHandler = value;
    }

    set respondedHandler(value: (e) => void) {
        this._respondedHandler = value;
    }

    set completeHandler(value: (e) => void) {
        this._completeHandler = value;
    }

    set cancelledHandler(value: (e) => void) {
        this._cancelledHandler = value;
    }
    get progressHandler(): (e) => void {
        return this._progressHandler;
    }

    get errorHandler(): (e) => void {
        return this._errorHandler;
    }

    get respondedHandler(): (e) => void {
        return this._respondedHandler;
    }

    get completeHandler(): (e) => void {
        return this._completeHandler;
    }
    get cancelledHandler(): (e) => void {
        return this._cancelledHandler;
    }
}

export class HttpHandler {
    statusCodeEnum = { '100': 'Continue',
        '101': 'Switching Protocols',
        '102': 'Processing',
        '200': 'OK',
        '201': 'Created',
        '202': 'Accepted',
        '203': 'Non-Authoritative Information',
        '204': 'No Content',
        '205': 'Reset Content',
        '206': 'Partial Content',
        '207': 'Multi-Status',
        '300': 'Multiple Choices',
        '301': 'Moved Permanently',
        '302': 'Moved Temporarily',
        '303': 'See Other',
        '304': 'Not Modified',
        '305': 'Use Proxy',
        '307': 'Temporary Redirect',
        '400': 'Bad Request',
        '401': 'Unauthorized',
        '402': 'Payment Required',
        '403': 'Forbidden',
        '404': 'Not Found',
        '405': 'Method Not Allowed',
        '406': 'Not Acceptable',
        '407': 'Proxy Authentication Required',
        '408': 'Request Time-out',
        '409': 'Conflict',
        '410': 'Gone',
        '411': 'Length Required',
        '412': 'Precondition Failed',
        '413': 'Request Entity Too Large',
        '414': 'Request-URI Too Large',
        '415': 'Unsupported Media Type',
        '416': 'Requested Range Not Satisfiable',
        '417': 'Expectation Failed',
        '418': 'I\'m a teapot',
        '422': 'Unprocessable Entity',
        '423': 'Locked',
        '424': 'Failed Dependency',
        '425': 'Unordered Collection',
        '426': 'Upgrade Required',
        '428': 'Precondition Required',
        '429': 'Too Many Requests',
        '431': 'Request Header Fields Too Large',
        '500': 'Internal Server Error',
        '501': 'Not Implemented',
        '502': 'Bad Gateway',
        '503': 'Service Unavailable',
        '504': 'Gateway Time-out',
        '505': 'HTTP Version Not Supported',
        '506': 'Variant Also Negotiates',
        '507': 'Insufficient Storage',
        '509': 'Bandwidth Limit Exceeded',
        '510': 'Not Extended',
        '511': 'Network Authentication Required' }
    config;
    httpModule = require("@nativescript/core/http");
    bgHttpModule = require("@nativescript/background-http");
    qsModule = require('qs');
    headers = {};
    responseMiddleWares = [];
    constructor(){
        this.config=WebServiceConfig;
    }
    _normalizeResource(url:String){
        if(url.startsWith("http://")||url.startsWith("https://")){
            return url;
        }
        else if(url.startsWith("//")){
            return "https:" + url;
        }
        else if(url.startsWith("/")){
            return this.config.endpointName + url.substr(1);
        }
        else {
            return this.config.endpointName + url;
        }
    }
    requestWithRefreshFallback(url, method, headers, content){
        return new Promise(async (resolve, reject)=>{
            // Prepare request
            let requestObj = {}
            if(url)
                requestObj["url"]=url;
            if(method)
                requestObj["method"] = method;
            if(headers)
                requestObj["headers"] = headers;
            if(content)
                requestObj["content"] = content;
            let response = await this.httpModule.request(requestObj);
            try{
                response.json = JSON.parse(response.content);
            }
            catch(e){

            }
            if(response.statusCode===403 && response.json.detail==="EXPIRED"){
                const store = getAppState();
                await store.dispatch("refreshTokens");
                response = await this.httpModule.request(requestObj);
                try{
                    response.json = JSON.parse(response.content);
                }
                catch(e){

                }
            }
            const finalResponse = response;
            resolve(finalResponse);
        });
    }
    async post(url:string, payload:object=null, urlFormEncoded:boolean=true){
        if(!payload)
            payload={}
        return this.endpointExceptionDetector(await this.requestWithRefreshFallback(
            this._normalizeResource(url),
            "POST",
            this.getHeaders(urlFormEncoded?{"content-type":"application/x-www-form-urlencoded"}:{}),
            urlFormEncoded?this.qsModule.stringify(payload):JSON.stringify(payload)
        ));
    }
    async get(url:string, payload:object=null){
        if(!payload)
            payload={}
        let normalizedUrl = this._normalizeResource(url) + "?" + this.qsModule.stringify(payload);
        return this.endpointExceptionDetector(await this.requestWithRefreshFallback(
            normalizedUrl,
            "GET",
            this.getHeaders(),
            null
        ))
    }
    async put(url:string, payload:object=null, urlFormEncoded:boolean=true){
        if(!payload)
            payload={}
        return this.endpointExceptionDetector(await this.requestWithRefreshFallback(
            this._normalizeResource(url),
            "PUT",
            this.getHeaders(urlFormEncoded?{"content-type":"application/x-www-form-urlencoded"}:{}),
            urlFormEncoded?this.qsModule.stringify(payload):JSON.stringify(payload)
        ))
    }
    async patch(url:string, payload:object=null, urlFormEncoded:boolean=true){
        if(!payload)
            payload={}
        return this.endpointExceptionDetector(await this.requestWithRefreshFallback(
            this._normalizeResource(url),
            "PATCH",
            this.getHeaders(urlFormEncoded?{"content-type":"application/x-www-form-urlencoded"}:{}),
            urlFormEncoded?this.qsModule.stringify(payload):JSON.stringify(payload)
        ));
    }
    async delete(url:string, payload:object=null, urlFormEncoded:boolean=true){
        if(!payload)
            payload={}
        return this.endpointExceptionDetector(await this.requestWithRefreshFallback(
            this._normalizeResource(url),
            "DELETE",
            this.getHeaders(urlFormEncoded?{"content-type":"application/x-www-form-urlencoded"}:{}),
            urlFormEncoded?this.qsModule.stringify(payload):JSON.stringify(payload)
        ));
    }
    async options(url:string){
        return this.endpointExceptionDetector(await this.requestWithRefreshFallback(
            this._normalizeResource(url),
            "OPTIONS",
            this.getHeaders({}),
            null
        ));
    }
    uploadFile(url:string, filePath:string, callBackHandler:UploadCallbackHandler,
               mimeType:string, fileParam:string="file", description:string="Uploading..."){
        const session = this.bgHttpModule.session("profile-image-upload");
        const request = {
            url: this._normalizeResource(url),
            method: "POST",
            headers: this.getHeaders({
                "Content-Type": "multipart/form-data"
            }),
            description: description,
            androidAutoDeleteAfterUpload: true,
            androidMaxRetries:3,
            androidAutoClearNotification: true
        };
        const params = [
            { name: fileParam, filename: filePath, mimeType: mimeType }
        ];
        const task = session.multipartUpload(params, request);
        task.on("progress", callBackHandler.progressHandler);
        task.on("error", callBackHandler.errorHandler);
        task.on("responded", callBackHandler.respondedHandler);
        task.on("complete", callBackHandler.completeHandler);
        task.on("cancelled", callBackHandler.cancelledHandler);
    }
    getHeaders(headers:object={}):object{
        let resultHeader = {}
        Object.assign(resultHeader, this.headers)
        Object.assign(resultHeader, headers)
        return resultHeader
    }
    setHeader(key, value:String){
        this.headers[key] = value;
    }
    removeHeader(key){
        delete this.headers[key];
    }
    addResponseMiddleware(middleware){
        this.responseMiddleWares.push(middleware);
    }
    clearResponseMiddlewares(){
        this.responseMiddleWares = [];
    }
    endpointExceptionDetector(response){
        for(let key in this.responseMiddleWares){
            try{
                this.responseMiddleWares[key].apply(this, [response]);
            }
            catch (e) {
                console.warn("Error middleware " + key +" raised another exception." );
                console.warn(e);
            }
        }

        if(String(response.statusCode).startsWith("5")){
            throw new ServerError((String(response.statusCode) in this.statusCodeEnum?this.statusCodeEnum[String(response.statusCode)]:"Unknown server error."), response);
        }
        else if(String(response.statusCode).startsWith("4")){
            throw new ClientError((String(response.statusCode) in this.statusCodeEnum?this.statusCodeEnum[String(response.statusCode)]:"Unknown client error."), response);
        }
        return response
    }

}

