import Vue from "nativescript-vue";

export const setHttpBearerToken = (token)=>{
    Vue.prototype.$http.setHeader("Authorization", "Bearer "+String(token));
}

export const removeHttpBearerToken = ()=>{
    Vue.prototype.$http.removeHeader("Authorization");
}