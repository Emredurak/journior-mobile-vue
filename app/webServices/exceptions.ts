export class ServerError extends Error {
    public response;
    public message;
    public name;
    constructor(message, response) {
        super(message); // (1)
        this.message=message;
        this.response = response
        this.name = "ServerError"; // (2)
    }
}

export class ClientError extends Error {
    public response;
    public message;
    public name;
    constructor(message, response) {
        super(message); // (1)
        this.message = message;
        this.response = response;
        this.name = "ClientError"; // (2)
    }
}