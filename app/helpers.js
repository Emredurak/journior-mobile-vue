const platformModule = require("@nativescript/core/platform");
const pageWidth = parseInt(platformModule.screen.mainScreen.widthDIPs);

export default {
    firstLetter(fullName,username) {
        fullName = fullName?fullName.trim():'';
        if (fullName || username) {
            let letter = fullName;
            if (letter === '') {
                letter = username.charAt(0);
            }
            let firstLetter = letter.charAt(0).toLowerCase();
            switch (firstLetter) {
                case 'ç': {
                    firstLetter = 'c2';
                    break;
                }
                case 'ı': {
                    firstLetter = 'i2';
                    break;
                }
                case 'ğ': {
                    firstLetter = 'g2';
                    break;
                }
                case 'ü': {
                    firstLetter = 'u2';
                    break;
                }
                case 'ö': {
                    firstLetter = 'o2';
                    break;
                }
                case 'ş': {
                    firstLetter = 's2';
                    break;
                }
            }
            return "res://"+firstLetter;
        }else{
            return 'res://unknown'
        }


    },

    /*
        this functions are helper functions for Agency News and News in 'Feed'
     */
    calculatePictures(pictures){
        // assume that all pictures has bigger width from device width
        // calculate heigth as same ratio to device width
        for(let i=0; i<pictures.length; i++){
            let calculatedHeight = this.calculateHeight(pictures[i].width, pictures[i].height, pageWidth);
            pictures[i].calculatedHeight = calculatedHeight;
        }
        return pictures;
    },
    calculateHeight(width, height){
        let ratio = width/height;
        let newHeight = pageWidth / ratio;
        return Math.round(newHeight);
    },
    /*
        pictures parameter should contains .calculatedHeight
     */
    getMaxHeight(pictures){
        let maxHeight = 0;
        for(let i=0; i<pictures.length; i++){
            if (pictures[i].calculatedHeight >= maxHeight){
                maxHeight = pictures[i].calculatedHeight;
            }
        }
        return maxHeight;
    },
    getMinHeight(pictures){
        let minHeight = pictures[0].calculatedHeight;
        for(let i=0; i<pictures.length; i++){
            if (pictures[i].calculatedHeight <= minHeight){
                minHeight = pictures[i].calculatedHeight;
            }
        }
        return minHeight;
    },
};
