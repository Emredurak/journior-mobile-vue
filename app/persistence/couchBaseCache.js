import {Couchbase, ConcurrencyMode} from 'nativescript-couchbase-plugin';
const CACHE_VERSION_NUMBER = 3;

const caches = {
    AgencyNewsCache: new Couchbase("AgencyNewsCache"),
    ProfileCache: new Couchbase("ProfileCache")
}

const component = {
    setUpCache: ()=>{
        let cacheMigrationDb = new Couchbase("cacheMigrationDb");
        let resultSet = cacheMigrationDb.query({
            select: [],
            limit: 1
        });
        if(resultSet.length>0){
            if(!(resultSet[0].version===CACHE_VERSION_NUMBER)){
                for(let k of Object.keys(caches)){
                    try{
                        caches[k].destroyDatabase();
                        caches[k] = new Couchbase(k);
                        console.log("Cache invalidated for " + k);
                    }
                    catch(e){
                        console.error(e);
                    }
                }
            }
        }
        cacheMigrationDb.createDocument({version:CACHE_VERSION_NUMBER}, "1");

    },
    ...caches
}

export default component;
