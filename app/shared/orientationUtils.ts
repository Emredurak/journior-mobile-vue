import {Screen} from "@nativescript/core/platform";
import {View} from "@nativescript/core/ui/";

export class OrientationHelper{
    private isInitialized:boolean=false;
    private parentView:View;
    public constructor(view:View){
        this.parentView = view;
    }
    public getOrientationBySize():string{
        if(Screen.mainScreen.widthPixels<Screen.mainScreen.heightPixels)
            return "portrait";
        return "landscape";
    }
    public initialize():void{
        if(!this.isInitialized){
            this.isInitialized = true;
            if(this.getOrientationBySize()=="landscape")
                this.setLandscape();
            else
                this.setPortrait();
            const application = require("@nativescript/core/application");
            const instance = this;
            application.on(application.orientationChangedEvent, function (args) {
                if(args.newValue=="landscape")
                    instance.setLandscape();
                else
                    instance.setPortrait();
            });
        }
    }
    private setLandscape():void{
        this.parentView.addPseudoClass("landscape");
    }
    private setPortrait():void{
        this.parentView.deletePseudoClass("landscape");
    }
}