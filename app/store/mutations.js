import {ApplicationSettings} from "@nativescript/core";

export const enableNavigationGestures= (state)=>{
    state.navigationGesturesEnabled=true;
}

export const disableNavigationGestures= (state)=>{
    state.navigationGesturesEnabled=false;
}

export const setCurrentOrientation = (state, value)=>{
    state.currentOrientation=value;
}

export const enableRenderBackStackPage = (state)=>{
    state.renderBackStackPage = true;
}

export const disableRenderBackStackPage = (state)=>{
    state.renderBackStackPage = false;
}

export const setLoggedIn = (state, value)=>{
    state.loggedIn=value;
}

export const setToken = (state, value)=>{
    state.token=value;
}

export const setRefreshToken = (state, value)=>{
    state.refreshToken=value;
}

export const setProfile = (state, value)=>{
    state.globalProfile = value;
}

export const enableProfileFetched = (state)=>{
    state.globalProfileFetched = true
}

export const disableProfileFetched = (state)=>{
    state.globalProfileFetched = false
}

export const setActiveTab = (state, value)=>{
    state.activeTab = value;
}

export const setRefreshFollowingList = (state, value)=>{
    state.refreshFollowingList = value;
}

export const setCacheProfileState = (state, value)=>{
    state.cacheProfileLoaded = value;
}

export const setSideBarOpened = (state, value)=>{
    state.sideBarOpened = value;
}

export const setMute = (state, value)=>{
    state.mute = value;
}

export const setFeedVisuallyLoaded = (state, value)=>{
    state.feedVisuallyLoaded = value;
}

export const setProfileDocumentID = (state, value)=>{
    ApplicationSettings.setString("cacheProfileID", value);
    state.profileDcoumentID = value;
}
