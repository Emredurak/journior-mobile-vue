import Vue from "nativescript-vue";
import {tokenSecureKeyName, refreshTokenSecureKeyName} from "../persistence/config";
import AccountRepository from "../repositories/accountRepository";
import {setHttpBearerToken, removeHttpBearerToken} from "../webServices/helpers";
import cache from "../persistence/couchBaseCache"

export const orientationChangeAction=(context, value)=>{
    context.commit("setCurrentOrientation", value);
};

export const loadLoginState=(context, {token, refreshToken})=>{
    return new Promise(async (resolve, reject)=>{
        if(token&&refreshToken){
            context.commit("setLoggedIn", true);
            context.commit("setToken", token);
            context.commit("setRefreshToken", refreshToken);
            setHttpBearerToken(token);
        }
        else{
            await Vue.prototype.$secureStorage.remove({key: tokenSecureKeyName});
            await Vue.prototype.$secureStorage.remove({key: refreshTokenSecureKeyName});
            context.commit("setLoggedIn", false);
            context.commit("setToken", null);
            context.commit("setRefreshToken", null);
            removeHttpBearerToken();
        }
        resolve();
    });
}

export const login=(context, {username, password}) => {
    return new Promise(async (resolve, reject) => {
        try{
            let response = await AccountRepository.login(username, password);
            await Vue.prototype.$secureStorage.set({
                key: tokenSecureKeyName,
                value: response.json.token
            });

            await Vue.prototype.$secureStorage.set({
                key: refreshTokenSecureKeyName,
                value: response.json.refresh_token
            });
            context.commit("setLoggedIn", true);
            context.commit("setToken", response.json.token);
            context.commit("setRefreshToken", response.json.refresh_token);
            setHttpBearerToken(response.json.token);
            resolve(response);
        }
        catch (e){
            console.log(e);
            reject(e);
        }
    });
}

export const logout=(context) => {
    return new Promise(async (resolve, reject) => {
        try {
            await Vue.prototype.$secureStorage.remove({key: tokenSecureKeyName});
            await Vue.prototype.$secureStorage.remove({key: refreshTokenSecureKeyName});
            context.commit("setLoggedIn", false);
            context.commit("setToken", null);
            context.commit("setRefreshToken", null);
            removeHttpBearerToken();
            resolve();
        }
        catch(e){
            console.log(e);
            reject(e);
        }
    });
}

export const refreshTokens=(context)=>{
    return new Promise(async (resolve, reject)=>{
        let response = await AccountRepository.refreshToken(await Vue.prototype.$secureStorage.get({
            key: refreshTokenSecureKeyName
        }));
        const token = response.json.token;
        const refreshToken = response.json.refresh_token;
        await Vue.prototype.$secureStorage.set({
            key: tokenSecureKeyName,
            value: token
        });

        await Vue.prototype.$secureStorage.set({
            key: refreshTokenSecureKeyName,
            value: refreshToken
        });

        context.commit("setToken", token);
        context.commit("setRefreshToken", refreshToken);
        setHttpBearerToken(token);
        resolve();
    });
}

export const fetchProfilePicture = (context)=>{
    return new Promise(async (resolve, reject)=>{
        try {
            const response = await AccountRepository.getAccountDetails();
            if(response.json){
                let currentUserData = context.getters.getProfile;
                currentUserData.profile.active_profile_picture =
                    response.json.profile.active_profile_picture;
                context.commit("setProfile", currentUserData);
                context.commit("enableProfileFetched");
            }
            else{
                reject(new Error("Cannot fetch profile."));
            }

            resolve(response.json);
        }
        catch(e){
            reject(e);
        }

    });
}

export const fetchProfile = (context)=>{
    return new Promise(async (resolve, reject)=>{
        try {
            const response = await AccountRepository.getAccountDetails();
            if(response.json){
                context.commit("setProfile", response.json);
                context.commit("enableProfileFetched");
                let profileCache = cache.ProfileCache;
                if(context.getters.getProfileDocumentID){
                    // profile already exist in cache, just update
                    profileCache.updateDocument(context.getters.getProfileDocumentID, response.json);
                }else{
                    // profile is being created first time in cache
                    // create document, update docID
                    // update cache profile informations
                    const documentId = profileCache.createDocument(response.json);
                    context.commit("setProfileDocumentID",documentId);
                    profileCache.updateDocument(context.getters.getProfileDocumentID, response.json);
                    context.commit("setCacheProfileState",true);
                }
            }
            else{
                reject(new Error("Cannot fetch profile."));
            }
            resolve(response.json);
        }
        catch(e){
            reject(e);
        }

    });
}

export const loadCacheProfile = (context)=>{
    try {
        let documentId = context.getters.getProfileDocumentID;
        console.log("Profile cache doc id:",documentId);
        if(documentId){
            const profile = cache.ProfileCache.getDocument(documentId);
            context.commit("setProfile",profile);
            context.commit("setCacheProfileState",true);
        }

    }
    catch (e) {
        console.log("Loading cache profile failed");
    }

}

