import Vue from 'nativescript-vue';
import Vuex from 'vuex';
import * as getters from './getters';
import * as mutations from './mutations';
import * as actions from './actions';

Vue.use(Vuex);
const appStore = [null];

export const getAppState = (loggedIn, token, refreshToken)=>{
    if(!appStore[0]){
        appStore[0] = new Vuex.Store({
            state: {
                navigationGesturesEnabled: true,
                currentOrientation: "",
                loggedIn: loggedIn,
                token: token,
                refreshToken: refreshToken,
                renderBackStackPage: true,
                globalProfile: {},
                globalProfileFetched: false,
                activeTab:0,
                refreshFollowingList:false,
                profileDcoumentID:"",
                cacheProfileLoaded:false,
                mute:true,
                feedVisuallyLoaded:false,
                sideBarOpened:false,
            },
            getters,
            mutations,
            actions
        });
    }
    return  appStore[0];
}





