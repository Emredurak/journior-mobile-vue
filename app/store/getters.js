import {ApplicationSettings} from "@nativescript/core";

export const getNavigationGesturesEnabled = (state)=>{
    return state.navigationGesturesEnabled;
}

export const getCurrentOrientation = (state)=>{
    return state.currentOrientation;
}

export const getRenderBackStackPage = (state)=>{
    return state.renderBackStackPage;
}

export const getToken = (state)=>{
    return state.token;
}

export const getRefreshToken = (state)=>{
    return state.refreshToken;
}

export const getLoggedIn = (state)=>{
    return state.loggedIn;
}

export const getProfile = (state)=>{
    return state.globalProfile;
}

export const getProfileFetchState = (state)=>{
    return state.globalProfileFetched;
}

export const getActiveTab = (state)=>{
    return state.activeTab;
}

export const getSideBarOpened = (state)=>{
    return state.sideBarOpened;
}

export const getRefreshFollowingList = (state)=>{
    return state.refreshFollowingList;
}

export const getCacheProfileState = (state)=>{
    return state.cacheProfileLoaded;
}

export const getMute = (state)=>{
    return state.mute;
}

export const getFeedVisuallyLoaded = (state)=>{
    return state.feedVisuallyLoaded;
}
export const getProfileDocumentID = (state)=>{
    if(ApplicationSettings.getString("cacheProfileID")) {
        return ApplicationSettings.getString("cacheProfileID");
    }else{
        return '';
    }
}