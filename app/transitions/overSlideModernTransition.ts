import * as transition from '@nativescript/core/ui/transition';
import { Screen } from '@nativescript/core/platform';
import {Frame} from "@nativescript/core/ui/frame";
import {Page} from "@nativescript/core/ui/page";
import lazy from '@nativescript/core/utils/lazy';

const screenWidth = lazy(() => Screen.mainScreen.widthPixels);
const screenHeight = lazy(() => Screen.mainScreen.heightPixels);
const POPEXIT_SLIDE_PERCENTAGE = 0.31;

export class OverSlideModernTransitionAndroid extends transition.Transition {
    private _direction: string;
    private _fromView: any;

    constructor(direction: string, duration: number, curve: any) {
        super(duration, curve);
        this._direction = direction;
    }

    public createAndroidAnimator(transitionType: string): android.animation.AnimatorSet {
        this._fromView = Frame.topmost().currentPage;
        const translationValues = Array.create('float', 2);
        switch (this._direction) {
            case 'left':
                switch (transitionType) {
                    case 'enter':
                        translationValues[0] = screenWidth();
                        translationValues[1] = 0;
                        break;
                    case 'popExit':
                        translationValues[0] = 0;
                        translationValues[1] = screenWidth();
                        break;
                }
                break;
            case 'right':
                switch (transitionType) {
                    case 'enter':
                        translationValues[0] = -screenWidth();
                        translationValues[1] = 0;
                        break;
                    case 'popExit':
                        translationValues[0] = 0;
                        translationValues[1] = -screenWidth();
                        break;
                }
                break;
            case 'top':
                switch (transitionType) {
                    case 'enter':
                        translationValues[0] = screenHeight();
                        translationValues[1] = 0;
                        break;
                    case 'popExit':
                        translationValues[0] = 0;
                        translationValues[1] = screenHeight();
                        break;
                }
                break;
            case 'bottom':
                switch (transitionType) {
                    case 'enter':
                        translationValues[0] = -screenHeight();
                        translationValues[1] = 0;
                        break;
                    case 'popExit':
                        translationValues[0] = 0;
                        translationValues[1] = -screenHeight();
                        break;
                }
                break;
        }

        let prop;

        if (this._direction === 'left' || this._direction === 'right') {
            prop = 'translationX';
        } else {
            prop = 'translationY';
        }

        const animator = android.animation.ObjectAnimator.ofFloat(null, prop, translationValues);
        const duration = this.getDuration();
        if (duration !== undefined) {
            animator.setDuration(duration);
        }
        animator.setInterpolator(this.getCurve());
        const as = new android.animation.AnimatorSet();
        let animationList:Array<android.animation.Animator>  = [animator];
        const transitionObject = this;
        ///Alpha animations.
        switch(transitionType){
            case 'popEnter':
                const alphaValuesReverseDown = Array.create('float', 2);
                alphaValuesReverseDown[0]=0.3;
                alphaValuesReverseDown[1]=1;
                const alphaAnimatorReverseDown = android.animation.ObjectAnimator.ofFloat(null, "alpha", alphaValuesReverseDown);
                alphaAnimatorReverseDown.setDuration(this.getDuration());
                alphaAnimatorReverseDown.setInterpolator(this.getCurve());
                animationList.push(alphaAnimatorReverseDown);
                //Slide values
                const slideValues = Array.create('float', 2);
                if(this._direction=="left"){
                    slideValues[0] = -POPEXIT_SLIDE_PERCENTAGE*screenWidth();
                    slideValues[1] = 0;
                }
                else if(this._direction=="right"){
                    slideValues[0] = POPEXIT_SLIDE_PERCENTAGE*screenWidth();
                    slideValues[1] = 0;
                }

                const slideAnimator = android.animation.ObjectAnimator.ofFloat(null, "translationX", slideValues);
                slideAnimator.setDuration(this.getDuration());
                slideAnimator.setInterpolator(this.getCurve());
                animationList.push(slideAnimator);
                break;
            case 'exit':
                const translationValues = Array.create('float', 2);
                translationValues[0]=0;
                translationValues[1]=1;
                const clipAnimator = android.animation.ValueAnimator.ofFloat(translationValues);
                const currentPage:Page = this._fromView;
                const nativeView = currentPage.nativeView;
                clipAnimator.addUpdateListener(new android.animation.ValueAnimator.AnimatorUpdateListener({
                    onAnimationUpdate(param0: android.animation.ValueAnimator): void {
                        switch (transitionObject._direction) {
                            case "left":
                                let slideAmount = POPEXIT_SLIDE_PERCENTAGE*param0.getAnimatedValue()*screenWidth();
                                nativeView.setTranslationX(-slideAmount);
                                nativeView.setClipBounds(new android.graphics.Rect(0, 0, screenWidth()*(1-param0.getAnimatedValue()) + slideAmount, screenHeight()));
                                break;
                            case "right":
                                let slideAmountRight = POPEXIT_SLIDE_PERCENTAGE*param0.getAnimatedValue()*screenWidth();
                                nativeView.setTranslationX(slideAmountRight);
                                nativeView.setClipBounds(new android.graphics.Rect(screenWidth()*(param0.getAnimatedValue())-slideAmountRight, 0, screenWidth(), screenHeight()));
                                break;
                            case "top":
                                nativeView.setClipBounds(new android.graphics.Rect(0, 0, screenWidth(), screenHeight()*(1-param0.getAnimatedValue())));
                                break;
                            case "bottom":
                                nativeView.setClipBounds(new android.graphics.Rect(0, screenHeight()*(param0.getAnimatedValue()), screenWidth(), screenHeight()));
                                break;
                        }
                    }
                }));
                clipAnimator.addListener(new android.animation.Animator.AnimatorListener({
                    onAnimationRepeat(param0: android.animation.Animator): void {
                    },
                    onAnimationCancel(param0: android.animation.Animator): void {
                    },
                    onAnimationEnd(param0: android.animation.Animator): void {
                        nativeView.setClipBounds(null);
                    },
                    onAnimationStart(param0: android.animation.Animator): void {
                    }
                }))
                animationList.push(clipAnimator);
                ///Alpha animations
                const alphaValues = Array.create('float', 2);
                alphaValues[0]=1;
                alphaValues[1]=0.3;
                const alphaAnimator = android.animation.ValueAnimator.ofFloat(alphaValues);
                alphaAnimator.addUpdateListener(new android.animation.ValueAnimator.AnimatorUpdateListener({
                    onAnimationUpdate(param0: android.animation.ValueAnimator): void {
                        currentPage.opacity = param0.getAnimatedValue()/1;
                    }
                }));
                alphaAnimator.addListener(new android.animation.Animator.AnimatorListener({
                    onAnimationCancel(param0: android.animation.Animator): void {
                    },
                    onAnimationRepeat(param0: android.animation.Animator): void {
                    },
                    onAnimationEnd(param0: android.animation.Animator): void {
                        currentPage.opacity=1;
                    },
                    onAnimationStart(param0: android.animation.Animator): void {
                    }
                }));
                alphaAnimator.setDuration(this.getDuration());
                alphaAnimator.setInterpolator(this.getCurve());
                animationList.push(alphaAnimator);
                break;
        }
        as.playTogether(java.util.Arrays.asList(animationList));
        return as;
    }
}